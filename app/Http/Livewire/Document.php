<?php

namespace App\Http\Livewire;

use App\Models\Document as ModelsDocument;
use App\Models\DocumentVersion;
use Exception;
use Illuminate\Support\Facades\DB;
use Livewire\Component;
use Illuminate\Support\Facades\Redis;
use Livewire\WithPagination;


class Document extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';

    public $search, $user_recent_document_redis_key, $title, $document, $tags, $decument_details;
    public function mount(){
        $this->user_recent_document_redis_key = "user_id:" . auth()->user()->id . ":recent:document";
    }
    public function render()
    {
        return view('livewire.document', [
            'cached_documents' => Redis::get($this->user_recent_document_redis_key) ?? [],
            'documents' => ModelsDocument::when($this->search, function ($q) {
                $q->where('title', 'like', "%$this->search%");
            })->paginate(10),
        ])->extends('master')->section('content');
    }

    public function details(ModelsDocument $document){
        // from cache
        $value = Redis::get('key');
        if($value){
            $this->decument_details = $document; //will removed
        }else{
            // from db
            // $this->decument_details = ModelsDocument::findOrFail($document_id);
            $this->decument_details = $document;
            Redis::set($this->user_recent_document_redis_key, $this->decument_details);
        }
    }

    public function submit()
    {
        $this->validate([
            'title' => 'required|string',
            'document' => 'required|array',
            'document.introduction' => 'required|string',
            'document.facts' => 'required|string',
            'document.summary' => 'required|string',
            'tags' => 'required|string',
        ]);

        DB::beginTransaction();
        try {
            if($this->selected_for_edit_document_id){
                $document =  ModelsDocument::find($this->selected_for_edit_document_id);
                $document->update([
                    'title' => $this->title,
                    'current_version' => "v-".$document->versions->count() + 1,
                    'status' => 'active'
                ]);
            }else{
                $document = ModelsDocument::create([
                    'title' => $this->title,
                    'current_version' => "v-1",
                    'status' => 'active'
                ]);
            }
            DocumentVersion::create([
                'document_id' => $document->id,
                'version' => $document->current_version,
                'body_content' => json_encode($this->document),
                'tags_content' => json_encode(explode(',', $this->tags)),
            ]);
            DB::commit();
            $this->reset();
            $this->dispatchBrowserEvent('alert', ['type' => 'success',  'message' => 'Successfully created !']);
        } catch (Exception $e) {
            DB::rollBack();
            $this->dispatchBrowserEvent('alert', ['type' => 'error',  'message' => $e->getMessage()]);
        }
    }


    // select_for_edit
    public $selected_for_edit_document_id;
    public function select_for_edit($document_id){
        $this->selected_for_edit_document_id = $document_id;
        $document =  ModelsDocument::find($this->selected_for_edit_document_id);
        if($document){
            $this->title = $document->title;
            $this->document['introduction'] = json_decode($document->latest_version->body_content)->introduction;
            $this->document['facts'] = json_decode($document->latest_version->body_content)->facts;
            $this->document['summary'] = json_decode($document->latest_version->body_content)->summary;
            $this->tags = implode (", ", json_decode($document->latest_version->tags_content));
        }else{
            $this->dispatchBrowserEvent('alert', ['type' => 'error',  'message' => 'Document not found']);
        }
    }

    public $selected_for_delete_document_id;
    public function select_for_delete($document_id){
        $this->selected_for_delete_document_id = $document_id;
    }

    public function delete(){
        $document =  ModelsDocument::find($this->selected_for_delete_document_id);
        if($document){
            $document->delete();
            $this->dispatchBrowserEvent('alert', ['type' => 'success',  'message' => 'Successfully deleted !']);
        }else{
            $this->dispatchBrowserEvent('alert', ['type' => 'error',  'message' => 'Document not found']);
        }
    }
}
