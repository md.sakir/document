<?php

namespace App\Console\Commands;

use App\Models\Document;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Redis;

class RedisGenerate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'redis:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'All documents with versiona and users set in redis';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        // Delete first
        Redis::flushdb();

        // Store new data from document mysql
        $items = Document::with('versions')->get()->toArray();
        $chunks = array_chunk($items, 1000);

        foreach ($chunks as $chunk) {
            Redis::pipeline(function ($pipe) use ($chunk) {
                foreach ($chunk as $item) {
                    // Perform Redis commands on each $item
                    $pipe->set('document:' . $item['id'], json_encode($item));
                    $pipe->incr('counter');
                }
            });
        }

        // Store new data from user mysql
        $items = User::all()->toArray();
        $chunks = array_chunk($items, 500);

        foreach ($chunks as $chunk) {
            Redis::pipeline(function ($pipe) use ($chunk) {
                foreach ($chunk as $item) {
                    // Perform Redis commands on each $item
                    $pipe->set('user:' . $item['id'], json_encode($item));
                    $pipe->incr('counter');
                }
            });
        }

    }
}
