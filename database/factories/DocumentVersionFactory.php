<?php

namespace Database\Factories;

use App\Models\Document;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\DocumentVersion>
 */
class DocumentVersionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'document_id' =>function () {
                return Document::class;
            },
            'version' => "v-1",
            'body_content' => json_encode([
                'introduction' => fake()->paragraph(),
                'facts' => fake()->paragraph(),
                'summary' => fake()->paragraph(),
            ]),
            'tags_content' => json_encode([
                fake()->word, fake()->word, fake()->word, fake()->word, fake()->word
            ]),
        ];
    }


}
