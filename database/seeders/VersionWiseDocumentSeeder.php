<?php

namespace Database\Seeders;

use App\Models\Document;
use App\Models\DocumentVersion;
use Illuminate\Database\Seeder;

class VersionWiseDocumentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // Create 2500 versions using 500 documents
        Document::factory(500)->create(['current_version' => "v-5"])->each(function ($document) {
            for ($i = 1; $i <= 5; $i++) {
                DocumentVersion::factory()->create(['document_id' => $document->id, 'version' => "v-$i"]);
            }
        });

        // Create 700 documents to complete total 1200 active/inactive documents
        Document::factory(700)->create()->each(function ($document) {
            $document->versions()->saveMany(DocumentVersion::factory(1)->make());
        });
    }
}
