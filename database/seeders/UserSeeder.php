<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        User::create([
            'author' => true,
            'name' => 'Author',
            'email' => 'author@example.com',
            'password' => bcrypt('password'),
        ]);

        User::create([
            'name' => 'Client',
            'email' => 'client@example.com',
            'password' => bcrypt('password'),
        ]);
    }
}
