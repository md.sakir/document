<?php

namespace Database\Seeders;

use App\Models\Document;
use App\Models\DocumentUser;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DocumentUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // Document Users: id, document_id, user_id, last_viewed_version
        foreach (User::where(['status' => 'active', 'author' => false])->get() as $user) {
            foreach (Document::whereStatus('active')->get() as $document) {
                foreach ($document->versions as $document_version) {
                    if(DocumentUser::count() >= 8400){
                        break 3; //exit from 3 loop
                    }
                    DocumentUser::create([
                        'document_id' => $document->id,
                        'user_id' => $user->id,
                        'last_viewed_version' => $document_version->version,
                    ]);
                }
            }
        }
    }
}
