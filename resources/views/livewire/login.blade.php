<div class="row justify-content-center">
    <div class="col-md-6">
        <div class="card mt-5">
            <div class="card-header text-center">
              LOGIN
            </div>
            <div class="card-body">
                <form wire:submit.prevent='login'>
                    <div class="mb-3">
                      <label class="form-label">Email address</label>
                      <input type="email" class="form-control" wire:model='email'>
                      @error('email') <x-alert :$message class="danger"/> @enderror
                    </div>
                    <div class="mb-3">
                      <label class="form-label">Password</label>
                      <input type="password" class="form-control" wire:model='password'>
                      @error('password') <x-alert :$message class="danger"/> @enderror
                    </div>
                    <button type="submit" class="btn btn-primary w-100">LOGIN</button>
                        <table class="table mt-5">
                            <thead class="thead-dark">
                                <tr>
                                    <th>Email</th>
                                    <th>Password</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>author@example.com</td>
                                    <td>password</td>
                                </tr>
                                <tr>
                                    <td>client@example.com</td>
                                    <td>password</td>
                                </tr>
                            </tbody>
                        </table>
                  </form>
            </div>
          </div>
    </div>
</div>
