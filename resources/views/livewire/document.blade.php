<div class="row">
    <div class="col-md-12 mt-5">
        <!-- Button trigger modal -->
        <div class="card">
            <h5 class="card-header">Featured {{ $search }}</h5>
            <div class="card-body">
                <div class="row justify-content-between m-2 border-bottom">
                    <div class="col-auto">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Search</span>
                            </div>
                            <input type="search" class="form-control" placeholder="Search here .." wire:model='search'>
                        </div>
                    </div>
                    @if (auth()->user()->author)
                        <div class="col-auto">
                            <button type="button" class="btn btn-primary" data-bs-toggle="modal"
                                data-bs-target="#document-modal">Create Document</button>
                        </div>
                    @endif
                </div>

                <div class="row mt-2 mb-2">
                    {{ print_r($cached_documents) }}
                    {{-- @foreach ($cached_documents as $key => $document)
                    <div class="col-md-3">
                            <div class="card">
                                <div class="card-body">
                                    {{ $document->title ?? '-' }}
                                </div>
                            </div>
                    </div>
                    @endforeach --}}
                </div>

                <table class="table">
                    <thead class="table-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Title</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($documents as $key => $document)
                            <tr>
                                <th scope="row">{{ $loop->iteration }}</th>
                                <td>{{ $document->title ?? '-' }}</td>
                                <td>
                                    <button class="btn btn-dark" data-bs-toggle="modal"
                                    data-bs-target=".document-details-modal" wire:click="details({{ $document }})">View Document</button>
                                    @if (auth()->user()->author)
                                    <button type="button" class="btn btn-warning" data-bs-toggle="modal" data-bs-target="#document-modal" wire:click="select_for_edit({{ $document->id }})">Edit</button>
                                    <button type="button" class="btn btn-danger" wire:click="select_for_delete({{ $document->id }})" data-bs-toggle="modal" data-bs-target=".document-delete-modal">Delete</button>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                    {{ $documents->links() }}
            </div>
        </div>

        @if (auth()->user()->author)
            <!-- Modal -->
            <div class="modal fade" id="document-modal" aria-hidden="true" wire:ignore.self>
                <div class="modal-dialog modal-dialog-centered">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="">Document form</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <div class="form-group mb-3">
                                <label for="title">Title</label>
                                <input type="text" class="form-control" id="title" wire:model='title'>
                                @error('title')
                                    <x-alert :$message class="danger" />
                                @enderror
                            </div>
                            <div class="form-group mb-3">
                                <label for="introduction">Introduction</label>
                                <textarea class="form-control" id="introduction" rows="3" wire:model='document.introduction'></textarea>
                                @error('document.introduction')
                                    <x-alert :$message class="danger" />
                                @enderror
                            </div>
                            <div class="form-group mb-3">
                                <label for="facts">Facts</label>
                                <textarea class="form-control" id="facts" rows="3" wire:model='document.facts'></textarea>
                                @error('document.facts')
                                    <x-alert :$message class="danger" />
                                @enderror
                            </div>
                            <div class="form-group mb-3">
                                <label for="summary">Summary</label>
                                <textarea class="form-control" id="summary" rows="3" wire:model='document.summary'></textarea>
                                @error('document.summary')
                                    <x-alert :$message class="danger" />
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="tags">Tags <small>(Use comma for separate tag)</small> </label>
                                <input type="text" class="form-control" id="tags" wire:model='tags'>
                                @error('tags')
                                    <x-alert :$message class="danger" />
                                @enderror
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-primary" wire:click='submit'>Save changes</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade document-delete-modal" aria-hidden="true" wire:ignore.self>
                <div class="modal-dialog modal-dialog-centered modal-sm">
                    <div class="modal-content justify-content-center text-center">
                        <img src="{{ asset('assets/images/delete.gif') }}">
                        <button class="btn btn-danger" wire:click='delete'>Delete Confirm</button>
                    </div>
                </div>
            </div>
        @endif

        <div class="modal fade document-details-modal" aria-hidden="true" wire:ignore.self>
            <div class="modal-dialog modal-dialog-centered modal-xl">
                <div class="modal-content">
                    @if($decument_details)
                    <div class="modal-header">
                        <h5 class="modal-title" id="">{{ $decument_details->title }}</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                            aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <nav>
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                               @foreach ($decument_details->versions as $version)
                                <button class="nav-link @if($loop->first) active @endif" data-bs-toggle="tab" data-bs-target="#nav-{{ $version->version }}" type="button" role="tab" aria-controls="nav-{{ $version->version }}" aria-selected="true">
                                    {{ $version->version }}
                                </button>
                              @endforeach
                            </div>
                        </nav>
                        <div class="tab-content mt-4" id="nav-tabContent">
                            @foreach ($decument_details->versions as $version)
                                <div class="tab-pane fade @if($loop->first) show active @endif" id="nav-{{ $version->version }}" role="tabpanel">
                                    <h6 class="card-subtitle mb-2 text-muted">Introduction</h6>
                                    <p class="card-text">{{ json_decode($version->body_content)->introduction }}</p>
                                    <h6 class="card-subtitle mb-2 text-muted">Facts</h6>
                                    <p class="card-text">{{ json_decode($version->body_content)->facts }}</p>
                                    <h6 class="card-subtitle mb-2 text-muted">Summary</h6>
                                    <p class="card-text">{{ json_decode($version->body_content)->summary }}</p>
                                    <h6 class="card-subtitle mb-2 text-muted">Tags</h6>
                                    @foreach (json_decode($version->tags_content) as $tag)
                                   <span class="badge bg-secondary">{{ $tag }}</span>
                                    @endforeach

                                </div>
                            @endforeach
                        </div>
                    </div>
                    @else
                    <h1 class="text-center">Not Found Yet</h1>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
