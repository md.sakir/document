<?php

use App\Http\Livewire\Document;
use App\Http\Livewire\Home;
use App\Http\Livewire\Login;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
Route::get('logout', function () {
    auth()->logout();
    return redirect()->route('login');
})->name('logout')->middleware('auth');


Route::get('/', Login::class)->name('login')->middleware('guest');
Route::get('home', Home::class)->name('home')->middleware('auth');
Route::get('document', Document::class)->name('document')->middleware('auth');


